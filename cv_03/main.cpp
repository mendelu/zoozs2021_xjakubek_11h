#include <iostream>

using namespace std;

class Programator {
private:
    string m_meno;
    int m_hodinovaMzda;
    int m_pocetHodin;
    int m_bonusZaNadcas;


public:
    Programator(string meno, int mzda, int bonus) {
        m_meno = meno;
        m_hodinovaMzda = mzda;
        m_bonusZaNadcas = bonus;
        m_pocetHodin = 0;
    }

    Programator(string meno) {
        m_meno = meno;
        m_pocetHodin = 0;
        m_hodinovaMzda = 500;
        m_bonusZaNadcas = 250;
    }

    void pracuj(int kolikHodin) {
        m_pocetHodin = m_pocetHodin + kolikHodin;
    }

    void koniecMesiaca() {
        m_pocetHodin = 0;
    }

    int vypocitajMzdu() {
        int mzda = m_pocetHodin * m_hodinovaMzda + vypocitajBonusKMzde();
        return mzda;
    }

    void printInfo() {
        cout << "Meno: " << m_meno << endl;
        cout << "Celkova mzda: " << vypocitajMzdu() << endl;
        cout << " --z toho bonus: " << vypocitajBonusKMzde() << endl;
    }

    ~Programator() {
        cout << "Programator " << m_meno << " zanikol :(." << endl;
    }

private:
    int vypocitajBonusKMzde() {
        if (m_pocetHodin >= 40) {
            return (m_pocetHodin - 40) * m_bonusZaNadcas;
        }
        return 0;
    }

};

int main() {
    Programator *mario = new Programator("Mario", 800, 10);
    mario->pracuj(8);
    mario->pracuj(8);
    mario->pracuj(8);
    mario->pracuj(8);
    mario->pracuj(9);
    mario->printInfo();
    mario->koniecMesiaca();
    mario->printInfo();

    delete mario;
    return 0;
}
