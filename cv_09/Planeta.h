//
// Created by xjakubek on 11.11.2021.
//

#ifndef ADVENTURA_PLANETA_H
#define ADVENTURA_PLANETA_H

#include "HraciPole.h"

class Planeta : public HraciPole {
    int m_ulozeneMnozstvoRudy;

public:
    Planeta(std::string popis, int mnozstvoRudy);

    int vytazDavkuRudy();
};


#endif //ADVENTURA_PLANETA_H
