cmake_minimum_required(VERSION 3.19.2)
project(adventura)

set(CMAKE_CXX_STANDARD 14)

add_executable(adventura main.cpp HraciPole.cpp HraciPole.h ObjektovaHraciDeska.cpp ObjektovaHraciDeska.h Planeta.cpp Planeta.h GameEngine.cpp GameEngine.h)
