//
// Created by xjakubek on 11.11.2021.
//

#ifndef ADVENTURA_GAMEENGINE_H
#define ADVENTURA_GAMEENGINE_H

#include "ObjektovaHraciDeska.h"
#include "Planeta.h"

class GameEngine {
    ObjektovaHraciDeska *m_deska;

public:
    GameEngine();

    void hraj();

    ~GameEngine();

private:
    void inicializujBunkyPola();

    void vypisMoznosti();
};


#endif //ADVENTURA_GAMEENGINE_H
