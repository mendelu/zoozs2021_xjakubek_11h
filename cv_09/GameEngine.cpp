//
// Created by xjakubek on 11.11.2021.
//

#include "GameEngine.h"

GameEngine::GameEngine() {
    m_deska = new ObjektovaHraciDeska(3, 3);
    inicializujBunkyPola();
}

void GameEngine::inicializujBunkyPola() {
    // naplnim prvy riadok
    m_deska->vloz(0, 0, new Planeta("saturn", 10));
    m_deska->vloz(0, 1, new Planeta("uran", 30));
    m_deska->vloz(0, 2, new Planeta("neptun", 15));

    // druhy riadok
    m_deska->vloz(1, 0, new Planeta("pluto", 1));
    m_deska->vloz(1, 1, new Planeta("bega", 2));
    m_deska->vloz(1, 2, new Planeta("io", 4));

    // treti riadok
    m_deska->vloz(2, 0, new Planeta("mars", 14));
    m_deska->vloz(2, 1, new Planeta("zem", 20));
    m_deska->vloz(2, 2, new Planeta("jupiter", 1));
}

void GameEngine::hraj() {
    int vstup = 0;

    do {
        std::cout << "Vyber, co chces udelat: ";
        std::cin >> vstup;

        switch (vstup) {
            case 1:
                vypisMoznosti();
                break;
            case 2:
                m_deska->vypisDesku();
                break;
            default:
                if (vstup != 0) {
                    std::cout << "Neznama volba!" << std::endl;
                }
                break;
        }

    } while (vstup != 0);
}

void GameEngine::vypisMoznosti() {
    std::cout << "------------" << std::endl;
    std::cout << "[0] koniec hry" << std::endl;
    std::cout << "[1] napoveda hry" << std::endl;
    std::cout << "[2] vypis hracej dosky" << std::endl;
    std::cout << "------------" << std::endl;
}

GameEngine::~GameEngine() {
    m_deska->vymazDesku();
    delete m_deska;
}