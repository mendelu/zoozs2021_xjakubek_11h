//
// Created by xjakubek on 11.11.2021.
//

#include "Planeta.h"

Planeta::Planeta(std::string popis, int mnozstvoRudy) : HraciPole(popis) {
    m_ulozeneMnozstvoRudy = mnozstvoRudy;
}

int Planeta::vytazDavkuRudy() {
    int davka = m_ulozeneMnozstvoRudy / 20;
    m_ulozeneMnozstvoRudy -= davka;
    return davka;
}


