#include <iostream>

using namespace std;

class ErrorLog {
private:
    string m_errors;
    static ErrorLog *s_log;
public:
    void logError(string what, string where) {
        m_errors += "- " + where + ": " + what + "\n";
    }

    string getErrors() {
        return m_errors;
    }

    static ErrorLog *getErrorLog() {
        if (s_log == nullptr) {
            s_log = new ErrorLog();
        }
        return s_log;
    }

private:
    ErrorLog() {
        m_errors = "ERROR LOGS: \n";
    }
};

ErrorLog *ErrorLog::s_log = nullptr;

class Drak {
private:
    float m_zdravie;
    float m_sila;
    float m_obrana;
public:
    Drak(float sila, float obrana) {
        setSila(sila);
        setObrana(obrana);
        m_zdravie = 100;
    }

    float getSila() {
        return m_sila;
    }

    float getZdravie() {
        return m_zdravie;
    }

    float getObrana() {
        return m_obrana;
    }

    void znizZdravie(float kolkoZdravia) {
        m_zdravie -= kolkoZdravia;
    }

private:
    void setSila(float sila) {
        if (sila <= 100) {
            m_sila = sila;
        } else {
            m_sila = 0;
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna sila.", "Trieda Drak");
        }
    }

    void setObrana(float obrana) {
        if ((obrana >= 0) and (obrana <= 200)) {
            m_obrana = obrana;
        } else {
            m_obrana = 0;
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna obrana.", "Trieda Drak");
        }
    }
};

class Rytier {
private:
    float m_zdravie;
    float m_sila;
    float m_obrana;
    string m_meno;
public:
    Rytier(float sila, float obrana, string meno) {
        m_sila = sila;
        m_obrana = obrana;
        m_zdravie = 100;
        m_meno = meno;
    }

    float getSila() {
        return m_sila;
    }

    float getZdravie() {
        return m_zdravie;
    }

    float getObrana() {
        return m_obrana;
    }

    string getMeno() {
        return m_meno;
    }

    void zautoc(Drak *nepriatel) {
        float silaNepriatela = nepriatel->getSila();
        float obranaNepriatela = nepriatel->getObrana();

        // 1: kontrola ci je rytier silnejsi
        // rytier ma vacsiu silu ako drak obranu
        if (m_sila > obranaNepriatela) {
            // vyhrava rytier -> znizim zivot drakovi
            nepriatel->znizZdravie(m_sila - obranaNepriatela);
        }

        // 2: kontrola ci je drak silnejsi
        // rytier ma mensiu obranu ako drak silu
        if (m_obrana < silaNepriatela) {
            // vyhrava drak -> znizim zivot rytiera
            m_zdravie -= silaNepriatela - m_obrana;
        }
    }

private:
    void setSila(float sila) {
        if (sila <= 100) {
            m_sila = sila;
        } else {
            m_sila = 0;
            cout << "Nespravne zadana sila: " << sila << endl;
        }
    }

    void setObrana(float obrana) {
        if ((obrana >= 0) and (obrana <= 200)) {
            m_obrana = obrana;
        } else {
            m_obrana = 0;
            cout << "Nespravna zadana obrana: " << obrana << endl;
        }
    }
};


int main() {
    Drak *smak = new Drak(150, 300);
    Rytier *artus = new Rytier(100, 90, "Artus");

    ErrorLog *log = ErrorLog::getErrorLog();
    cout << log->getErrors() << endl;

    artus->zautoc(smak);
    cout << "Rytier: " << artus->getZdravie() << endl;
    cout << "Drak: " << smak->getZdravie();

    delete smak;
    delete artus;
    return 0;
}
