//
// Created by xjakubek on 04.11.2021.
//

#ifndef CV_08_SYSTEM_H
#define CV_08_SYSTEM_H

#include "Patient.h"

class System {
    static std::vector<Patient *> s_patients;
    static int s_patientsCount;

public:
    static void createPatient(std::string name);

    static Patient *findPatient(int id);
};


#endif //CV_08_SYSTEM_H
