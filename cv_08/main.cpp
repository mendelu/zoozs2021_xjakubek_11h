#include <iostream>

#include "System.h"

int main() {
    System::createPatient("Tomas");
    System::createPatient("Denisa");
    System::createPatient("Honza");

    auto patient = System::findPatient(1);
    if (patient != nullptr) {
        patient->printInfo();
    }

    return 0;
}
