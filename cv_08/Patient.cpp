//
// Created by xjakubek on 04.11.2021.
//

#include "Patient.h"

Patient::Patient(std::string name, int id) {
    m_name = name;
    m_id = id;
    generateTeeth();
}

int Patient::getId() {
    return m_id;
}

std::array<std::array<Tooth *, 16>, 2> Patient::getTeeth() {
    return m_teeth;
}

void Patient::printInfo() {
    std::cout << "Name: " << m_name << std::endl;
    std::cout << "ID: " << m_id << std::endl;
    std::cout << "Upper diagnoses count: " << calcDgsCountOnJaw(0) << std::endl;
    std::cout << "Lower diagnoses count: " << calcDgsCountOnJaw(1) << std::endl;
}

int Patient::calcDgsCountOnJaw(int index) {
    int count = 0;
    for (auto *tooth: m_teeth.at(index)) {
        count += tooth->getDiagnoses().size();
    }
    return count;
}

void Patient::generateTeeth() {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 16; j++) {
            m_teeth.at(i).at(j) = new Tooth("Molar");
//            m_teeth[i][j] = new Tooth("Molar");
        }
    }
}