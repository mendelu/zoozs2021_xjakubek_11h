//
// Created by xjakubek on 04.11.2021.
//

#include "Tooth.h"

Tooth::Tooth(std::string type) {
    m_type = type;
    m_diagnoses = {};
}

std::vector<Diagnosis *> Tooth::getDiagnoses() {
    return m_diagnoses;
}

void Tooth::addDiagnoses(std::vector<Diagnosis *> newDgs) {
    for (auto *dg:newDgs) {
        m_diagnoses.push_back(dg);
    }
//    m_diagnoses.insert(m_diagnoses.end(), newDgs.begin(), newDgs.end());
}