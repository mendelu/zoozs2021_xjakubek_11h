//
// Created by xjakubek on 04.11.2021.
//

#ifndef CV_08_PATIENT_H
#define CV_08_PATIENT_H

#include <iostream>
#include <array>
#include "Tooth.h"

class Patient {
    std::string m_name;
    int m_id;
    std::array<std::array<Tooth *, 16>, 2> m_teeth;

public:
    Patient(std::string name, int id);

    int getId();

    std::array<std::array<Tooth *, 16>, 2> getTeeth();

    void printInfo();

private:
    void generateTeeth();

    int calcDgsCountOnJaw(int index);
};


#endif //CV_08_PATIENT_H
