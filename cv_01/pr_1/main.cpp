#include <iostream>

using namespace std;

class Sportovec {
public:
    string m_meno;
    float m_celkomKm;

    void setMeno(string noveMeno) {
        m_meno = noveMeno;
    }

    void behaj(float kolkoKm) {
        m_celkomKm = m_celkomKm + kolkoKm;
//        m_celkomKm += kolkoKm;
    }

    string getMeno() {
        return m_meno;
    }

    void printInfo() {
        cout << "Meno: " << m_meno << endl;
        cout << "Celkom km: " << m_celkomKm << endl;
    }

};

int main() {
    Sportovec *sport1 = new Sportovec();
    sport1->setMeno("Tomas");
    sport1->behaj(5);
    sport1->behaj(3);
    sport1->printInfo();

    return 0;
}

