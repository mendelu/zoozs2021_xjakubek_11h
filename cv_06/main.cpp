#include <iostream>
#include <array>
#include <vector>

using namespace std;

class Box {
    float m_weight;
    string m_content;
    string m_owner;

public:
    Box(float w, string c, string o) {
        m_weight = w;
        m_content = c;
        m_owner = o;
    }

    string getContent() {
        return m_content;
    }

    string getOwner() {
        return m_owner;
    }
};

class Floor {
    string m_label;
    array<Box *, 10> m_boxes;

public:
    Floor(string label) {
        m_label = label;
        for (int i = 0; i < m_boxes.size(); ++i) {
            m_boxes.at(i) = nullptr;
        }
    }

    void storeBox(Box *box, int index) {
        // kontrola indexu
        if ((index >= 0) and (index < m_boxes.size())) {
            // kontrola obsadenosti
            if (m_boxes.at(index) == nullptr) {
                // nic na tom indexe neni -> mozem ulozit
                m_boxes.at(index) = box;
            } else {
                cout << "There is a box at position " << index << "!\n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void removeBox(int index) {
        // kontrola indexu
        if ((index >= 0) and (index < m_boxes.size())) {
            // kontrola obsadenosti
            if (m_boxes.at(index) != nullptr) {
                // na tom indexe nieco je -> mozem to odstranit
                m_boxes.at(index) = nullptr;
            } else {
                cout << "There is no box at position " << index << "!\n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void printInfo() {
        cout << endl << "Floor " << m_label << " state: " << endl;
        for (auto *currentBox:m_boxes) {
            if (currentBox != nullptr) {
                cout << currentBox->getContent() << endl;
            } else {
                cout << "Position is empty!" << endl;
            }
        }
    }
};

class Store {
    vector<Floor *> m_floors;
public:
    Store() {
        m_floors.push_back(new Floor("Floor n.0"));
    }

    void buildNewFloor() {
        string floorLabel = "Floor n." + to_string(m_floors.size());
        Floor *newFloor = new Floor(floorLabel);
        m_floors.push_back(newFloor);
//    m_floors.push_back(new Floor("Floor n." + to_string(m_floors.size())))
    }

    void destroyLastFloor() {
        delete (m_floors.at(m_floors.size() - 1));
        m_floors.pop_back();
    }

    void storeBox(int floor, int position, Box *box) {
        // TODO kontrola indexov
        m_floors.at(floor)->storeBox(box, position);
    }

    void printInfo() {
        for (auto *currentFloor:m_floors) {
            currentFloor->printInfo();
        }
    }

    ~Store() {
        for (auto *floor:m_floors) {
            delete (floor);
        }
    }
};


int main() {
    auto *box = new Box(500, "korytnacky", "Roman");
    auto *store = new Store();
    store->buildNewFloor();
    store->buildNewFloor();
    store->buildNewFloor();

    store->storeBox(2, 7, box);
    store->printInfo();

    delete box;
    delete store;
    return 0;
}
