#include <iostream>

using namespace std;

/*
 * Trieda evidujuca baliky.
 */
class Balik {
public:
    float m_vaha;
    string m_prijemca;
    string m_odosielatel;
    int m_dobierka;

    Balik(string prijemca, string odosielatel, float vaha,
          int dobierka) {
        m_prijemca = prijemca;
        m_odosielatel = odosielatel;
//        m_vaha = vaha;
        setVaha(vaha);
        m_dobierka = dobierka;
    }

    Balik(string prijemca, string odosielatel, float vaha) :
            Balik(prijemca, odosielatel, vaha, 0) {}

    Balik(string prijemca, string odosielatel) :
            Balik(prijemca, odosielatel, 0.0, 0) {}

    Balik(string prijemca, string odosielatel, int dobierka) :
            Balik(prijemca, odosielatel, 0.0, dobierka) {}

    void printInfo() {
        cout << "Prijemca: " << m_prijemca << endl;
        cout << "Odosielatel: " << m_odosielatel << endl;
        cout << "Vaha: " << m_vaha << endl;
        cout << "Dobierka: " << m_dobierka << endl;
    }

    void setVaha(float vaha) {
        if (vaha < 0) {
            // -1, -2, ...
            m_vaha = 0;
            cout << "Zadal si nevalidnu vahu!!!" << endl;
        } else {
            // 0, 1, ...
            m_vaha = vaha;
        }
    }

    void nahradPrijemcu() {
        m_prijemca = m_odosielatel;
    }


};

int main() {
    Balik *balik1 = new Balik("Tomas", "Zuzana", 5000);
    balik1->setVaha(-10);
    balik1->printInfo();

    cout << "--------------------" << endl;

    Balik *balik2 = new Balik("David", "Martina", -10, 0);
    balik2->printInfo();

    return 0;
}
